﻿

namespace Solution
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class Application : System.Windows.Application
    {
        public Application()
        {
            InitializeComponent();
        }
    }
}
