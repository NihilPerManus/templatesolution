﻿
using Solution.MVVM.ViewModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Solution._compositionRoot
{
    public static class CompositionRoot
    {
        static Container container;

  
        [STAThread]
        public static void Main() {

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            container = new Container();

            //SAVE
            Bootstrap(); //Standard things to show the window. Generally don't need to touch this.... ever
            CustomInjections(); // App-specific classes go here. These arethings that are specific to the application you are currently writing
            container.Verify(); //Verify. This step Just informs you that your container configuration has potential issues. 
            Execute(); //Execute the application. If you didn't touch the boot-strap, you don't need to touch this. 

        }

        private static void Execute()
        {
            //Load up an instance of the application. This stupidly has to be run first before messing with any UI components
            Application app = new Application() { ShutdownMode = System.Windows.ShutdownMode.OnLastWindowClose};

            //Get the built Mainwindow that the container provides (with all injections ect.)
            MainWindow window = container.GetInstance<MainWindow>();
            //Set the datacontext to the viewmodel. This lets the Xaml know what you're talking about when it's trying to bind to objects that it has no idea about
            window.DataContext = container.GetInstance<IMainWindowViewModel>();

            //run the application
            app.Run(window);

        }

        private static void CustomInjections()
        {
            //place your custom injections in here.







        }

        private static void Bootstrap()
        {
            //Registers the mainwindow and its viewmodel
            container.Register<MainWindow>();
            container.Register<IMainWindowViewModel, MainWindowViewModel>();
            
        }
    }
}
